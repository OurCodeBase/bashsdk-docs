# bash-sdk 🔥
A bash library to create standalone scripts.

## Features ✨
There are some features of bash-sdk are mentioned here.
* OOPS like code 💎.
* Module based code 🗂️.
* Similar functions to python 🐍.
* Standalone script creation 📔.

## Beauty 🏵️
Checkout the ui of this cli project [here](/general/ui).

## General 🍷
There are some rules or things to keep in mind while using this library.

The rules are mentioned [here](/general/).

## Installation 🌀
Just clone it to anywhere.
```sh
git clone --depth=1 https://github.com/OurCodeBase/bash-sdk.git
```

## Modules 📚
These are the modules created in bash-sdk library. You can read about their functions by clicking on them.
* [ask.sh](/docs/ask)
* [cursor.sh](/docs/cursor)
* [db](/docs/db)
* [file.sh](/docs/file)
* [inspect.sh](/docs/inspect)
* [os.sh](/docs/os)
* [package.sh](/docs/package)
* [repo.sh](/docs/repo)
* [say.sh](/docs/say)
* [screen.sh](/docs/screen)
* [spinner.sh](/docs/spinner)
* [string.sh](/docs/string)
* [url.sh](/docs/url)

## Structure 🗃️
File structure of bash-sdk is like:
```sh
bash-sdk
├── docs # docs for bash-sdk.
├── _uri.sh # helper of builder.
├── builder.sh
└── src
    ├── ask.sh
    ├── cursor.sh
    ├── db.sh
    ├── file.sh
    ├── inspect.sh
    ├── os.sh
    ├── package.sh
    ├── repo.sh
    ├── say.sh
    ├── screen.sh
    ├── spinner.sh
    ├── string.sh
    └── url.sh
```

## Compiler 🧭
Compiler does combine all codes in a standalone bash file.
```sh
bash builder.sh -i "path/to/input.sh" -o "path/to/output.sh";
```
* input file is the file that you are going to compile.
* output file is the standalone result file.

Then you can directly execute output file without bash-sdk library.

## Queries 📢
If you have any questions or doubt related to this library you can directly ask everything [here](https://github.com/OurCodeBase/bash-sdk/issues).

## Suggestion 👌
* [bash-lsp](https://github.com/bash-lsp/bash-language-server) to your code editor to get auto completions.
* [shellcheck](https://github.com/koalaman/shellcheck) to debug bash code.
* [cooked.nvim](https://github.com/OurCodeBase/cooked.nvim) code editor to get best compatibility.

## Author 🦋
* Created By [@OurCodeBase](https://github.com/OurCodeBase)
* Inspired By [@mayTermux](https://github.com/mayTermux)