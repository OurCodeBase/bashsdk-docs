# General
These are some general rules of bash scripting you should to know about.

## Shell Script
If you don't know anything about shell or bash scriping you can learn about it in [freeCodeCamp](https://www.freecodecamp.org/news/bash-scripting-tutorial-linux-shell-script-and-command-line-for-beginners/).

## Calling Functions
You should to call functions as
```bash
Car(){
  echo "The car is ${1}";
}

# Wrong way
Car Mercedes Benz

# Right way
Car "Mercedes Benz"
```
output:
```bash
# output of wrong way
The car is Mercedes
# output of right way
The car is Mercedes Benz
```

## Calling Modules
You can call modules as
```sh
source path/to/bash-sdk/src/say.sh
```
In this example we are calling `say` module in our code. In your case, modules can be different.

And the path `path/to/` is mentioned here as path where you placed the bash-sdk library.

If you are ready then click to [Getting Started](/#modules-📚) to work with modules.

## STRIP
STRIP is a global returnable `array` variable. If a function have to return you an array then it uses `STRIP` variable to serve data.

Demo:
```go
file.readlines "path/to/file.txt";
```
This function is used to read a file and give you all lines in an array. When you call the function with argument, then it automatically send data to `STRIP` array variable.

We are going to print all data in `STRIP` variable.
```go
for var in "${STRIP[@]}";
do
  echo "${var}";
done
```

## How to use ?
This is how you can use defined functions.

Demo: 
```bash
var1="Hello World";
var2="$(text.replace "${var1}" "World" "Car")";
echo "${var2}";
```
Result:
```
Hello Car
```
Demo 2:
```bash
var1="$(screen.cols)";
echo "${var1}";
```
Result:
```
86
```

## Boolean Value
The functions which are returning `bool` values are basically giving you `exit codes` or `return codes` in 0 and 1.

Demo:
```bash
path.isdir "path/to/dir";
echo $?
```
In this demo `path.isdir` function is going to check that directory is exists or not. Suppose if the directory is exist.

Result:
```
0
```