export default {
  base: '/bashsdk-docs/',
  outDir: '../public',
  title: 'OurCodeBase',
  description: 'A docs for bash-sdk',
  themeConfig: {
    siteTitle: "bash-sdk",
    socialLinks: [
      { icon: "github", link: "https://github.com/OurCodeBase" },
      { icon: "instagram", link: "https://instagram.com/itspandapoo" },
      { icon: "youtube", link: "https://m.youtube.com/@OurCodeBase" }
    ],
    sidebar: [
      {
        text: "General 🍷",
        collapsible: true,
        items: [
          { text: "Shell Script", link: "/general/#shell-script" },
          { text: "Calling Functions", link: "/general/#calling-functions" },
          { text: "Calling Modules", link: "/general/#calling-modules" },
          { text: "STRIP", link: "/general/#strip" },
          { text: "How to use ?", link: "/general/#how-to-use" },
          { text: "Boolean Value", link: "/general/#boolean-value" },
        ],
      },
      {
        text: "Modules 📚",
        collapsible: true,
        items: [
          { text: "ask.sh", link: "/docs/ask" },
          { text: "cursor.sh", link: "/docs/cursor" },
          { text: "db.sh", link: "/docs/db" },
          { text: "file.sh", link: "/docs/file" },
          { text: "inspect.sh", link: "/docs/inspect" },
          { text: "os.sh", link: "/docs/os" },
          { text: "package.sh", link: "/docs/package" },
          { text: "repo.sh", link: "/docs/repo" },
          { text: "say.sh", link: "/docs/say" },
          { text: "screen.sh", link: "/docs/screen" },
          { text: "spinner.sh", link: "/docs/spinner" },
          { text: "string.sh", link: "/docs/string" },
          { text: "url.sh", link: "/docs/url" },
        ],
      },
    ],
  },
};
