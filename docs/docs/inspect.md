# Inspect Module
This module is used to ask for required functions or things.

## inspect.ScreenSize
```go
inspect.ScreenSize(cols,lines) -> str
  Checks that screen size is sufficient to project.
Args:
  cols (int) > takes columns as arg.
  lines (int) > takes lines as arg.
```
Showcase :
![ScreenSize](/inspect.ScreenSize.png)

## inspect.is_func
```go
inspect.is_func(function) -> str
  An extension of os.is_func.
```