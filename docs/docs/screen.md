# Screen Module
This module is related to screen.

## screen.cols
```go
screen.cols() -> int
  Gives you current columns count in terminal.
```

## screen.lines
```go
screen.lines() -> int
  Gives you current lines count in terminal.
```

## screen.isSize
```go
screen.isSize(cols,lines) -> bool
  Checks that screen has atleast given lines and columns.
Args:
  cols (int) > takes columns as int.
  lines (int) > takes lines as int.
```