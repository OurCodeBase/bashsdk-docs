# String Module
This module is used to manipulate strings.

## text.randize
```go
text.randize(array) -> obj
  Gives you random object of array.
Args:
  array (array) > takes array of objects.
```

## text.isdigit
```go
text.isdigit(str)
  Checks string is digit or not.
Args:
  str (str) > takes string as arg.
```

## text.replace
```go
text.replace(str,old,neu)
  This replace string to string.
Args:
  str (str) > takes string as arg.
  old (str) > takes string to replace.
  neu (str) > takes string to replace with.
```

## text.len
```go
text.len(str) -> int
  Gives you lenth of given string.
```

## text.len.strip
```go
text.len.strip(str) -> int
  Gives you count of lines in given string.
Args:
  str (str) > takes string as arg.
```

## text.startwith
```go
text.startwith(str,startstr) -> bool
  Checks that string startswith given substring or not.
Args:
  str (str) > takes string as arg.
  startstr (str) > takes substring as arg.
```

## text.endswith
```go
text.endswith(str,endstr) -> bool
  Checks that string endswith given substring or not.
Args:
  str (str) > takes string as arg.
  endstr (str) > takes substring as arg.
```

## text.contains
```go
text.contains(str,contain) -> bool
  Checks that string contains substring or not.
Args:
  str (str) > takes string as arg.
  contain (str) > takes charachter.
```

## text.charCount
```go
text.charCount(str,char) -> int
  Gives you count of a given character in given string.
Args:
  str (str) > takes string as arg.
  char (char) > takes charachter.
```

## text.replace_charAt
```go
text.replace_charAt(str,pos,char) -> str
  Replace character of a string at given pos.
Args:
  str (str) > takes string as arg.
  pos (int) > takes position.
  char (char) > takes charachter.
```