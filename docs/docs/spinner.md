# Spinner Module
This module is regarding to spinner.
```go
Shorts:
  spinner.start(use,subject)
  run your functions.
  spinner.stop
```

Showcase :
![spinner](/spinner.png)

## spinner.setCursor
```go
spinner.setCursor(on~off)
  Switch terminal cursor easily.
```

## spinner.start
```go
spinner.start(use,subject)
  Starts spinner to spin.
Args:
  use (str) > takes process (eg: installing, processing).
  subject (str) > takes subject (eg: file, function).
Means:
  (use, subject) > (Processing 'Sleep')
```

## spinner.stop
```go
spinner.stop()
  Stops spinner to spin.
```