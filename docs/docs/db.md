# DB Module
This module is used to manipulate `Yaml` files as database.
Provide us `CRUD` operations via `key` and `value` using  `Yaml` file.

* It supports only yml files.

## db.read
```go
db.read(key,file) -> str
  Give you data of key of yaml db.
Args:
  key (str) > takes key of db.
  file (str) > takes file path.
```

## db.create
```go
db.create(key,value,file)
  Adds data key and value to db.
Args:
  key (str) > takes key of db.
  value (str) > takes value of key.
  file (str) > takes file path.
```

## db.update
```go
db.update(key,value,file)
  Update data of key in db file.
Args:
  key (str) > takes key of db.
  value (str) > takes update value of key.
  file (str) > takes file path.
```

## db.delete
```go
db.delete(key,file)
  Delete key and value of db file.
Args:
  key (str) > takes key of db.
  file (str) > takes file path.
```