# Package Module
This module is related to packages.

## pkg.size
```go
pkg.size(dnload~install,package) -> int
  Gives you needed size of package.
Args:
  dnload (str) > dnload to get file size.
  install (str) > install to get package installed size.
  package (str) > takes package (eg: python,nodejs).
Returns:
  size (int) > Gives you size in MBs.
Usage:
  pkg.size(dnload,package) > Gives you package file size.
  pkg.size(install,package) > Gives you package installed size.
```

## pkg.chart
```go
pkg.chart(pkgs)
  Use to view chart of packages.
Args:
  pkgs (array) > takes array of packages.
```

## pkg.install
```go
pkg.install(packages)
  Used to install packages with good ui.
Args:
  packages (array) > takes packages as args. (eg: python nodejs neovim)
```
Showcase :
![pkg.install](/pkg.install.png)