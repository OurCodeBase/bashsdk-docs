# Cursor Module
This can be used to manipulate terminal cursor.

## setCursor
```go
setCursor(on~off)
  Switch terminal cursor easily.
```