# Repo Module
This module is regarding to github repository.

## repo.size
```go
repo.size(api) -> str
  Used to get size of a repo.
Args:
  api (str) > takes api of github repo. (eg: "OurCodeBase/bash-sdk")
Returns:
  size (int) > gives you file size in MB. (eg: 30 MB)
```

## repo.chart
```go
repo.chart(apis)
  Used to view info of given repositories.
Args:
  apis (array) > takes array of repository api.
```

## repo.clone
```go
repo.clone(apis,*dirs)
  Used to start cloning of a repository.
Args:
  apis (array) > takes apis of github repo. (eg: OurCodeBase/bash-sdk)
  dirs (array) > Optional: You can give directory path to clone to it.
```
Showcase :
![repo.clone](/repo.clone.png)