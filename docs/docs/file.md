# File Module
This can be used to manipulate files.

## path.isdir
```go
path.isdir(dir) -> bool
  Checks that directory exist or not.
Args:
  dir (str) > takes directory path.
```

## path.isfile
```go
path.isfile(file) -> bool
  Checks that file exist or not.
Args:
  file (str) > takes file path.
```

## file.move
```go
file.move(src,dst)
  This can use to move files.
Args:
  src (str) > takes source path.
  dst (str) > takes destination path.
```

## file.copy
```go
file.copy(src,dst)
  This can use to copy files.
Args:
  src (str) > takes source path.
  dst (str) > takes destination path.
```

## file.erase
```go
file.erase(file)
Args:
  file (str) > takes file path.
```

## file.pop
```go
file.pop(pos,file)
  Popout given position of line in file.
Args:
  pos (int) > takes position.
  file (str) > takes file path.
```

## file.readlines.int
```go
file.readlines.int(file) -> int
  Gives you total lines of a file.
Args:
  file (str) > takes file path.
```

## file.readline
```go
file.readline(pos,file) -> str
  Gives you line from given position of file.
Args:
  pos (int) > takes position.
  file (str) > takes file path.
```

## file.readlines
```go
file.readlines(file) -> STRIP.
  Gives you STRIP array of lines of given file.
Args:
  file (str) > takes file path.
```

## file.readline.tall
```go
file.readline.tall(file) -> str
  Gives you largest line of file.
Args:
  file (str) > takes file path.
```

## file.replace.pos
```go
file.replace.pos(str,pos,file)
  This replace text from given line of file.
Args:
  str (str) > takes string to replace.
  pos (int) > takes position.
  file (str) > takes file path.
```

## file.search
```go
file.search(str,file) -> str
  Search given text in file & return you that line.
Args:
  str (str) > takes string to search.
  file (str) > takes file path.
```

## file.search.pos
```go
file.search.pos(str,file) -> pos
  Search given text in file & return you position (eg: 1,2).
Args:
  str (str) > takes string to search.
  file (str) > takes file path.
```

## path.dirArray
```go
path.dirArray(dir,*options) -> STRIP.
  Gives you array of files in given directory.
Args:
  dir (str) > takes directory path.
  --by-time (obj) > Optional: list will be sorted by time.
  --no-extension (obj) > Optional: list have no file extensions.
```
For example
```bash
path.dirArray 'path/to/dir' --no-extension --by-time
```

## file.append.hori
```go
file.append.hori(str,pos,file)
  This appends text to file horizontally.
Args:
  str (str) > takes string to append it.
  pos (int) > takes position.
  file (str) > takes file path.
```

## file.append.vert
```go
file.append.vert(str,pos,file)
  This appends text to file vertically.
Args:
  str (str) > takes string to append it.
  pos (int) > takes position.
  file (str) > takes file path.
```