# Url Module
This module is basically for downloading contents.

## url.contentSize
```go
url.contentSize(url) -> int
  Gives you size of content file in MBs.
Args:
  url (str) > takes url as string.
Returns:
  size (int) > size in MBs (eg: 60).
```

## url.contentChart
```go
url.contentChart(urls,*paths)
  This is used to chart urls and size.
Args:
  urls (array) > takes one or array of urls.
  paths (array) > Optional: takes file paths.
```

## url.getContent
```go
url.getContent(urls,*paths)
  This is used to get files via urls.
Args:
  urls (array) > takes one or array of url.
  paths (array) > Optional: takes files path.
```