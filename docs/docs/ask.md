# Ask Module
This module is basically for taking user inputs.

## ask.case
```go
ask.case(title) -> bool
  This takes case ( yes or no ) for any work.
Args:
  title (str) > takes title (eg: You're Agree).
```
Showcase :
![ask.case](/ask.case.png)

## ask.choice
```go
ask.choice(title,list) -> var
  This creates a simple menu.
Args:
  title (str) > takes title (eg: Choose One).
  list (array) > takes array as arg.
Returns:
  object (str) > result in 'askChoice' & 'askReply' variables.
```