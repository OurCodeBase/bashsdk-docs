# Say Module
This module is used to print texts in syntax highlighting way and contains prompts like error, warning, and success.

## say.gencolor
```go
say.gencolor() -> str
  Gives you a random color code.
```

## say
```go
say(str) -> str
  This prints given text with syntax highlighting feature.
```

## say.error
```go
say.error(str) -> str
  Says error to terminal.
```

## say.warn
```go
say.warn(str) -> str
  Says warning to terminal.
```

## say.success
```go
say.success(str) -> str
  Says success to terminal.
```

## say.checkStatus
```go
say.checkStatus(status) -> str
  This prints success or failure according to exit code.
Args:
  status (int) > takes exit code.
```