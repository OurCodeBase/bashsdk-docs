# Os Module
This module is related to Os or platform.

## os.is_userland
```go
os.is_userland() -> bool
  OS is userland or not ?
```

## os.is_termux
```go
os.is_termux() -> bool
  OS is termux or not ?
```

## os.is_windows
```go
os.is_windows() -> bool
  OS is windows or not ?
```

## os.is_shell.zsh
```go
os.is_shell.zsh() -> bool
  Using z-shell or not ?
```

## os.is_func
```go
os.is_func(function) -> bool
  Checks that function is exist or not.
Args:
  function (str) > takes function as string.
```